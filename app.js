const {Transaction, Blockchain} = require('./blockchain');
const EC = require('elliptic').ec;
const ec = new EC('secp256k1');

const myKey = ec.keyFromPrivate('c0633c518aa5f6b086bf9c83c22874181937367d8bbcc930f201e50c93ca2c3f');
const myWalletAddress = myKey.getPublic('hex');


let blockchain = new Blockchain();

const tx1 = new Transaction(myWalletAddress, 'public key goes here', 10);
tx1.signTransaction(myKey);
blockchain.addTransaction(tx1);


console.log("Starting mining");
blockchain.minePendingTransactions(myWalletAddress);

console.log('Starting mining');
blockchain.minePendingTransactions(myWalletAddress);

console.log(`Balance of address3 is ${blockchain.getBalanceOfAddress(myWalletAddress)}`);

console.log(`Get transactions for ${myWalletAddress}`);

for (const block of blockchain.chain){
    for (const tx of block.transactions){
      if (tx.fromAddress===myWalletAddress || tx.toAddress === myWalletAddress){
          console.log(tx);
      }
    }
}

